package kz.kaznu.oop.models;


import javax.persistence.*;

@Entity
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

//    @ElementCollection(targetClass = User.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user", joinColumns = @JoinColumn(name = "id"))
    private int user_id;

    private String status;
//    @ElementCollection(targetClass = Img.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "img", joinColumns = @JoinColumn(name = "id"))
    private int img_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }
}
