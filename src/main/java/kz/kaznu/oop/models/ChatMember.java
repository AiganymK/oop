package kz.kaznu.oop.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ChatMember {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int chat_id;

    private int user_id;

    private String status, date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
