package kz.kaznu.oop.models;

import javax.persistence.*;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

//    @ElementCollection(targetClass = User.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user", joinColumns = @JoinColumn(name = "id"))
    private int user_id;

//    @ElementCollection(targetClass = Chat.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "chat", joinColumns = @JoinColumn(name = "id"))
    private int chatId;

//    @ElementCollection(targetClass = Img.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "img", joinColumns = @JoinColumn(name = "id"))
    private int img_id;

    private String date, text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }

    public int getChat_id() {
        return chatId;
    }

    public void setChat_id(int chat_id) {
        this.chatId = chat_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
