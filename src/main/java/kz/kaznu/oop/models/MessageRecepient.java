package kz.kaznu.oop.models;

import javax.persistence.*;

@Entity
public class MessageRecepient {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

//    @ElementCollection(targetClass = User.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user", joinColumns = @JoinColumn(name = "id"))
    private int user_id;

//    @ElementCollection(targetClass = Message.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "message", joinColumns = @JoinColumn(name = "id"))
    private int message_id;

    private boolean is_read;

    private String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public boolean isIs_read() {
        return is_read;
    }

    public void setIs_read(boolean is_read) {
        this.is_read = is_read;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
