package kz.kaznu.oop.repo;

import kz.kaznu.oop.models.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatRepository extends JpaRepository<Chat, Integer> {
}
