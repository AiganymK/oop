package kz.kaznu.oop.repo;

import kz.kaznu.oop.models.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {
}
