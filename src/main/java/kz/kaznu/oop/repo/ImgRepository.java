package kz.kaznu.oop.repo;

import kz.kaznu.oop.models.Img;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImgRepository extends JpaRepository<Img, Integer> {
}
