package kz.kaznu.oop.repo;

import kz.kaznu.oop.helpers.DateHelper;
import kz.kaznu.oop.models.Img;
import kz.kaznu.oop.models.Profile;
import kz.kaznu.oop.models.Role;
import kz.kaznu.oop.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, Integer> {
    default void addUser(User user, MultipartFile file, String uploadPath, ImgRepository imgRepository, ProfileRepository profileRepository) throws IOException {
        user.setDate(DateHelper.getDate());
        user.setIs_active(true);
        user.setRoles(Collections.singleton(Role.USER));
        this.save(user);

        Profile profile = new Profile();
        profile.setUser_id(user.getId());
        profile.setStatus("");

        if(file != null){
            File uploadFolder = new File("C:/Users/фора/Downloads/");
            if(!uploadFolder.exists()){
                uploadFolder.mkdir();
            }
            String uuid = UUID.randomUUID().toString();
            String result = uuid + "." +file.getOriginalFilename();

            file.transferTo(new File("C:/Users/фора/Downloads/" + result));
            Img img = new Img();
            img.setPath(result);
            img.setDate(DateHelper.getDate());
            imgRepository.save(img);
            profile.setImg_id(img.getId());
        }

        profileRepository.save(profile);
    }

    User findByLogin(String login);
}
