package kz.kaznu.oop.controllers;

import kz.kaznu.oop.helpers.DateHelper;
import kz.kaznu.oop.models.Img;
import kz.kaznu.oop.models.Profile;
import kz.kaznu.oop.models.User;
import kz.kaznu.oop.repo.ChatRepository;
import kz.kaznu.oop.repo.ImgRepository;
import kz.kaznu.oop.repo.ProfileRepository;
import kz.kaznu.oop.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Controller
public class MainController {
    @Autowired
    UserRepository userRepository;

    @Value("${upload.path}")
    String uploadPath;

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    ImgRepository imgRepository;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/add")
    public String showForm(User user) {
        System.out.println(uploadPath);return "form";
    }

    @PostMapping("/add")
    public String checkPersonInfo(User user, BindingResult bindingResult, @RequestParam("file") MultipartFile file) throws IOException {
        User userFromDb = userRepository.findByLogin(user.getLogin());
        if (userFromDb != null) {
            ObjectError error = new ObjectError("login", "User Exists");
            bindingResult.addError(error);
            System.out.println(error);
            return "form";
        }

        if (bindingResult.hasErrors()) {
            return "form";
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.addUser(user, file, uploadPath, imgRepository, profileRepository);

        return "redirect:/login";
    }

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/hello")
    public String hello(Map<String, Object> model) {
        model.put("chats", chatRepository.findAll());
        return "hello";
    }

//    @GetMapping("/changeProfile")
//    public String change(Profile profile) {
//        return "profileForm";
//    }
//
//    @PostMapping("/changeProfile")
//    public String changePost(Profile profile, BindingResult bindingResult) {
//        User userFromDb = userRepository.findByLogin(profile.getLogin());
//        if (userFromDb != null) {
//            ObjectError error = new ObjectError("login", "User Exists");
//            bindingResult.addError(error);
//            System.out.println(error);
//            return "form";
//        }
//        if (bindingResult.hasErrors()) {
//            return "form";
//        }
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
//        userRepository.addUser(user);
//
//        return "redirect:/login";
//    }
}