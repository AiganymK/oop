package kz.kaznu.oop.controllers;

import kz.kaznu.oop.helpers.DateHelper;
import kz.kaznu.oop.models.Chat;
import kz.kaznu.oop.models.Img;
import kz.kaznu.oop.models.Message;
import kz.kaznu.oop.repo.ChatRepository;
import kz.kaznu.oop.repo.ImgRepository;
import kz.kaznu.oop.repo.MessageRepository;
import kz.kaznu.oop.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Controller
public class ChatController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    ImgRepository imgRepository;

    @Autowired
    MessageRepository messageRepository;

    @GetMapping("/createChat")
    public String showForm(Chat chat) {
        return "chatForm";
    }

    @PostMapping("/createChat")
    public String checkPersonInfo(Chat chat, BindingResult bindingResult, MultipartFile file) throws IOException {
//        User userFromDb = userRepository.findByLogin(user.getLogin());
//        if (userFromDb != null) {
//            return "chatForm";
//        }
        if (bindingResult.hasErrors()) {
            return "chatForm";
        }
        if(file != null){
            File uploadFolder = new File("C:/Users/фора/Downloads/oop/oop/src/main/resources/static/img");
            if(!uploadFolder.exists()){
                uploadFolder.mkdir();
            }
            String uuid = UUID.randomUUID().toString();
            String result = uuid + "." +file.getOriginalFilename();

            file.transferTo(new File("C:/Users/фора/Downloads/oop/oop/src/main/resources/static/img/" + result));
            Img img = new Img();
            img.setPath(result);
            img.setDate(DateHelper.getDate());
            imgRepository.save(img);
            chat.setImg_id(img.getId());
        }
        chat.setDate(DateHelper.getDate());
        chatRepository.save(chat);

        return "redirect:/hello";
    }

    @GetMapping("/chat")
    public String chat(Map<String, Object> model, Integer chat_id) {
        Optional<Chat> foo = chatRepository.findById(chat_id);
        model.put("chat", foo.get().getName());
        model.put("id", chat_id);
        model.put("messages", messageRepository.findByChatId(chat_id));
        return "chat";
    }

    @PostMapping("/chat")
    public String chats(Message message, BindingResult bindingResult, MultipartFile file) throws IOException {
//        User userFromDb = userRepository.findByLogin(user.getLogin());
//        if (userFromDb != null) {
//            return "chatForm";
//        }
        if(file != null){
            File uploadFolder = new File("C:/Users/фора/Downloads/oop/oop/src/main/resources/static/img");
            if(!uploadFolder.exists()){
                uploadFolder.mkdir();
            }
            String uuid = UUID.randomUUID().toString();
            String result = uuid + "." +file.getOriginalFilename();

            file.transferTo(new File("C:/Users/фора/Downloads/oop/oop/src/main/resources/static/img/" + result));
            Img img = new Img();
            img.setPath(result);
            img.setDate(DateHelper.getDate());
            imgRepository.save(img);
            message.setImg_id(img.getId());
        }
        message.setDate(DateHelper.getDate());
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        message.setUser_id(userRepository.findByLogin(name).getId());
        messageRepository.save(message);
        if (bindingResult.hasErrors()) {
            return "hello";
        }

        return "redirect:/hello";
    }
}